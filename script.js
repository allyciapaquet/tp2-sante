let maintenant = new Date().toLocaleString()
let nbreJoursTotal = parseInt(prompt("Combien de jours voulez-vous analyser?"))
const nom = "Allycia"
const sexeUser = 'F'
const SituationParticuliere = "non"

function getUserAge(date){
    var diff = Date.now() - date.getTime()
    var age = new Date(diff)
    return Math.abs(age.getUTCFullYear() - 1970)
}

console.log(maintenant)
console.log(nom)
console.log(getUserAge(new Date(1999, 11, 31)) +" " + "ans")
console.log(sexeUser)
console.log(SituationParticuliere)
console.log(nbreJoursTotal + " " + "jours")
alert("L'analyse de votre consommation sera de" + " " + nbreJoursTotal + " " + "jours.")

// calcule alcool

let journeeAlcool =[]
let jourSansAlcool = []
let jourExcedant = []

for(let i = 1; i <= nbreJoursTotal; i++){
    let nbreConsommationAlcool = prompt("Entrez votre nombre de consommation d'alcool pour la journée" + " " + i)
    nbreConsommationAlcool = parseInt(nbreConsommationAlcool)
    journeeAlcool.push(nbreConsommationAlcool)
    if(nbreConsommationAlcool == 0){
        jourSansAlcool.push(nbreConsommationAlcool)
    } if (nbreConsommationAlcool > recommandationParJour()) {
        jourExcedant.push(nbreConsommationAlcool)
    }
}

console.log(jourExcedant.length + " " + "exces")

console.log(journeeAlcool)

// recommandation par jour

function recommandationParJour(){
    if (SituationParticuliere == "oui"){
        let recommandationJour = 0
        return recommandationJour
    } if (sexeUser == 'H') {
        let recommandationJour = 3
        return recommandationJour
    } else {
        let recommandationJour = 2
        return recommandationJour        
    }
}

// recommandation par semaine

function recommandationParSemaine(){
    if (SituationParticuliere == "oui"){
        let recommandationSemaine = 0
        return recommandationSemaine
    } if (sexeUser == "H") {
        let recommandationSemaine = 15
        return recommandationSemaine
    } else {
        let recommandationSemaine = 10
        return recommandationSemaine        
    }
}

// calcule 

function ratioJourneeExcedant(){
    return jourExcedant.length * 100 / nbreJoursTotal
}

const maxParJour = Math.max(...journeeAlcool)

let sommeAlcool =  (accumulator, currentValue) => accumulator + currentValue
let maxParSemaine = (journeeAlcool.reduce(sommeAlcool))

if (nbreJoursTotal < 7 || nbreJoursTotal > 7) {
    let resultatMaxParSemaine = (maxParSemaine/nbreJoursTotal)*7
    console.log(resultatMaxParSemaine.toFixed(2)+ " " + "(la somme ramener sur 7 jours)") 
}

const maxConsoHomme = maxParJour == 3 || (maxParSemaine || resultatMaxParSemaine) == 15
const maxConsoFemme = maxParJour == 2 || (maxParSemaine || resultatMaxParSemaine) == 10

// recommandation

function recommandationRespecteeOuPas(){
    if (SituationParticuliere == "oui"){
        return "Vous ne respectez pas les recommandations"
}
else if(sexeUser == "H" && maxParJour > 3 || maxParSemaine > 15){
    return "Vous ne respectez pas les recommandations"
}
else if(sexeUser == "F" && maxParJour > 2 || maxParSemaine > 10){
    return "Vous ne respectez pas les recommandations"
}
else{
    return "Vous respectez les recommandations"
}}

// moyenne,ratio, rapport

function ratio(valeur,  total){
    return (valeur*100/total)
}

function calculerMoyenne(nombreConsommationTotal, nbreJoursTotal){
    return nombreConsommationTotal / nbreJoursTotal
}

function rapport(texte, valeur){
    console.log(texte + " " + ":" + " " +valeur)
}

rapport("Alcool","")
rapport("Moyenne par jour", calculerMoyenne(maxParSemaine, nbreJoursTotal).toFixed(2))
rapport("Consommation sur une semaine", maxParSemaine)
// rapport("Recommandation", 15)//pour les hommes
rapport("Recommandation", 10)//pour les femmes
rapport("Maximum en une journée", Math.max(...journeeAlcool))
// rapport("Recommandation", 3)//pour les hommes
rapport("Recommandation", 2)//pour les femmes
rapport("Ratio des journées excédants", ratioJourneeExcedant().toFixed(2) + ' %')
rapport("Ratio de journée sans alcool", ratio(jourSansAlcool.length, nbreJoursTotal).toFixed(2)+" " + "%")
   
console.log(recommandationRespecteeOuPas()) 